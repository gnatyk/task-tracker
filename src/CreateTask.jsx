import React, { Component } from 'react';
import { Button, Modal, Form } from 'semantic-ui-react';
import ProjectsService from './services/ProjectsService';
import UsersService from './services/UsersService';



class CreateTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nameTask: '',
      descrTask: '',
      developerList: [],
      selectedDeveloper: ''
    }
    this.getTaskName = this.getTaskName.bind(this);
    this.createNewTask = this.createNewTask.bind(this);
    this.getDescrTask = this.getDescrTask.bind(this);
    this.setSelectedDeveloper = this.setSelectedDeveloper.bind(this);
  }
  getTaskName(e) {
    let state = this.state;
    state.nameTask = e.target.value;
    this.setState(state);
  }
  componentWillReceiveProps() {
    let state = this.state;
    state.nameTask = '';
    state.descrTask = '';
    state.selectedDeveloper = '';
    state.developerList = [];
    this.setState(state);
    let self = this;
    ProjectsService.getProjectDevelopers(this.props.projectId, function (developers) {
      for (let i = 0; i < developers.length; i++) {
        UsersService.getUserById(developers[i].userId, function (user) {
          let state = self.state;
          state.developerList.push(user);
          self.setState(state);
        })
      }
    })
  }
  getDescrTask(e) {
    let state = this.state;
    state.descrTask = e.target.value;
    this.setState(state);
  }
  createNewTask() {
    this.props.onCreate(this.state.nameTask, this.state.descrTask, this.state.selectedDeveloper);
    this.props.onClose();
  }
  setSelectedDeveloper(e, option) {
    let state = this.state;
    state.selectedDeveloper = option.value;
    this.setState(state);
  }

  render() {
    return (
      <Modal size={'small'} open={this.props.isOpened} onClose={this.props.onClose}>
        <Modal.Header>
          Create new Task
          </Modal.Header>
        <Modal.Content>
          <Form as="div">  
            <Form.Group widths='equal'>
              <Form.Input label='Task summary' placeholder='Enter task summary' value={this.state.nameTask} onChange={this.getTaskName} />
              <Form.Select label='Assignee'
                options={
                  this.state.developerList.map((developer) => {
                    return {
                      key: developer.id,
                      value: developer.id,
                      text: developer.name
                    }
                  })}
                value={this.state.selectedDeveloper}
                onChange={this.setSelectedDeveloper} placeholder='Assignee' />
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.TextArea label='Task description' placeholder='Enter task description' value={this.state.descrTask} onChange={this.getDescrTask} />
            </Form.Group>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button basic onClick={() => this.props.onClose()}>
            Cancel
              </Button>
          <Button positive onClick={this.createNewTask} disabled={!this.state.nameTask || !this.state.descrTask}>
            Submit
            </Button>
        </Modal.Actions>
      </Modal>
    );
  }

}

export default CreateTask;