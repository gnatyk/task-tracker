import React, { Component } from 'react';
import { Grid, Header, Icon, Label, Button } from 'semantic-ui-react';
import TasksService from './services/TasksService';
import TaskEditor from './TaskEditor';
import UsersService from './services/UsersService';

class TaskInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      task: {},
      isModalOpened: false,
      assigneeTask: {}

    }
    this.taskEditorOpen = this.taskEditorOpen.bind(this);
    this.taskEditorClose = this.taskEditorClose.bind(this);
    this.getTask = this.getTask.bind(this);
    this.updateTask = this.updateTask.bind(this);
  }
  componentWillMount() {
    this.getTask();
  }
  taskEditorOpen() {
    let state = this.state;
    state.isModalOpened = true;
    this.setState(state);
  }
  taskEditorClose() {
    let state = this.state;
    state.isModalOpened = false;
    this.setState(state);
  }
  getTask() {
    let self = this;
    var taskId = self.props.taskId;
    TasksService.getTaskById(taskId, function (task) {
      UsersService.getUserById(task.assigneeId, function(user){
        let state = self.state;
        state.task = task;
        state.assigneeTask = user || {name: 'Unassigned'}
        self.setState(state);
      })
    });
  }
  updateTask(name, description, assigneId, status) {
    let self = this;
    TasksService.updateTask(self.props.taskId, name, assigneId, status, description, function result() {
      self.getTask();
  })
}

render() {
   let defaultImage = 'http://react.semantic-ui.com/assets/images/wireframe/white-image.png';
  return (
    <Grid>
      <Grid.Row>
        <Grid.Column width={16}>
          <Header as='h2' icon textAlign='center'>
            <Icon name='bookmark' circular />
            <Header.Content>
              {this.state.task.name}
            </Header.Content>
          </Header>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width={16}>
          <Button icon basic size="small" floated="right" onClick={this.taskEditorOpen}><Icon name="pencil" /></Button>
          <TaskEditor isOpened={this.state.isModalOpened} onClose={this.taskEditorClose} task={this.state.task} updateTask={this.updateTask} />
          <div>
            <span><b>Assignee </b></span>
            <Label as='a' image>
              
              <img role="presentation" src={this.state.assigneeTask.photo || defaultImage} />
              {this.state.assigneeTask.name}
            </Label>
          </div>
          <br />
          <div>
            <span><b>Status </b></span>
            <Label color="green">{this.state.task.status}</Label>
          </div>
          <br />
          <div>
            <div><b>Description</b></div>
            <p>
              {this.state.task.description}
            </p>
          </div>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}

}

export default TaskInfo;