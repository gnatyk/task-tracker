import React, { Component } from 'react';
import { Container } from 'semantic-ui-react';
import Navbar from './Navbar';


class App extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <Container text>
          {this.props.children}
        </Container>
      </div>
    );
  }
}

export default App;
