import React, { Component } from 'react';
import { Button, Grid, Header, Icon, Menu,Checkbox } from 'semantic-ui-react';
import { browserHistory } from 'react-router';
import ProjectsService from './services/ProjectsService';
import TasksService from './services/TasksService';
import CreateTask from './CreateTask';
import UsersService from './services/UsersService'

class Project extends Component {
    constructor(props) {
        super(props);
        this.state = {
            project: {},
            tasks: [],
            createModalOpened: false,
            currentUser: {},
            showOnlyMyTasks: false
        }
        this.getTasks = this.getTasks.bind(this);
        this.createNewTask = this.createNewTask.bind(this);
        this.openCreateModal = this.openCreateModal.bind(this);
        this.closeCreateModal = this.closeCreateModal.bind(this);
        this.deleteTask = this.deleteTask.bind(this);
        this.onDevClick = this.onDevClick.bind(this);
        this.onTaskClick = this.onTaskClick.bind(this);
    }
    openCreateModal() {
        let state = this.state;
        state.createModalOpened = true;
        this.setState(state);
    }
    closeCreateModal() {
        let state = this.state;
        state.createModalOpened = false;
        this.setState(state);
    }
    createNewTask(name, description, assigneeId) {
        var self = this;
        TasksService.create(name, description, assigneeId, localStorage.userId, self.props.params.id, function () {
            self.getTasks(self.props.params.id);
        })
    }
    onDevClick() {
        let id = this.props.params.id;
        browserHistory.push('/project/' + id + '/developers');
    }
    onTaskClick() {
        let id = this.props.params.id;
        browserHistory.push('/project/' + id + '/tasks');
    }
    isActiveTab(path) {
        var reg = new RegExp('/project/[0-9]+/' + path);
        var matches = reg.exec(browserHistory.getCurrentLocation().pathname);
        return matches != null;
    }
    componentWillMount() {
        this.getTasks(this.props.params.id);
        let self = this;
        ProjectsService.getProjectById(this.props.params.id, function (project) {
            let state = self.state;
            state.project = project;
            self.setState(state);
        });
        UsersService.getUserById(localStorage.userId, function (user) {
            self.setState({ currentUser: user });
        })
    }
    getTasks(id) {
        let self = this;
        if (!this.state.showOnlyMyTasks) {
            TasksService.getAllTasks(id, function (tasks) {
                let state = self.state;
                state.tasks = tasks;
                self.setState(state);
            })
        }
        else {
            TasksService.getAllTasksForUser(id, localStorage.userId, function (tasks) {
                let state = self.state;
                state.tasks = tasks;
                self.setState(state);
            })
        }
    }
    deleteTask(id) {
        var self = this;
        TasksService.deleteTask(id, function () {
            self.getTasks();
        })
    }
    filterChanged(data) {
        this.setState({ showOnlyMyTasks: data }, () => {
            this.getTasks(this.props.params.id);
        });
    }

    render() {
        return (
            <Grid>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Header as='h2' icon textAlign='center'>
                            <Icon name='browser' circular />
                            <Header.Content>
                                Project - {this.state.project.name}
                            </Header.Content>
                        </Header>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column >
                        {
                            this.state.currentUser.role === 'Developer' ?
                                <Checkbox toggle label='Only My Tasks'
                                    checked={this.state.showOnlyMyTasks}
                                    onChange={(e, data) => { this.filterChanged(data.checked) }} />
                                : null
                        }
                        <Button floated="right" positive icon onClick={this.openCreateModal}>
                            <Icon name='plus' /> New task
                        </Button>
                        <CreateTask onCreate={this.createNewTask} isOpened={this.state.createModalOpened} onClose={this.closeCreateModal} projectId={this.props.params.id} />
                    </Grid.Column >
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Menu pointing secondary>
                            <Menu.Item name='Tasks' active={this.isActiveTab("tasks")} onClick={this.onTaskClick} />
                            <Menu.Item name='Developers' active={this.isActiveTab("developers")} onClick={this.onDevClick} />
                        </Menu>
                        {
                            React.Children.map(this.props.children,
                                (child) => React.cloneElement(child, {
                                    project: this.state.project,
                                    tasks: this.state.tasks,
                                    onDelete: this.deleteTask
                                })
                            )}
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}

export default Project;