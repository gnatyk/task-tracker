import React, { Component } from 'react'
import {  Dropdown, Menu, Image} from 'semantic-ui-react';
import { browserHistory } from 'react-router';
import UsersService from './services/UsersService';

class Navbar extends Component {
     constructor(props) {
         super(props);
         this.state = {
             user: {
                 name: '',
                 photo: ''
             }
         }
     }
    logOut(){
       delete localStorage.userId;
       window.location.href = '/';
    }
    componentWillMount(){
        var id = localStorage.userId;
        let self = this;
        UsersService.getUserById(id, function(user){
            let state = self.state;
            state.user.name = user.name;
            state.user.photo = user.photo;
            self.setState(state);
        });
    }
    render() {
        let imageSrc = this.state.user.photo || 'http://react.semantic-ui.com/assets/images/wireframe/white-image.png';
        const profileBtn =
            (
                <span>
                    <Image src={imageSrc} avatar />
                    {this.state.user.name}
                </span>
            );
        return (
            <Menu >
                <Menu.Item name='Task Tracker' onClick={()=> browserHistory.push("/")}/>
                <Menu.Menu position='right'>
                    <Dropdown trigger={profileBtn} pointing='top right' item>
                        <Dropdown.Menu>
                            <Dropdown.Item text='Account' icon='user' onClick={()=> browserHistory.push("/profile")} />
                            <Dropdown.Item text='Sign Out' icon='sign out' onClick={this.logOut} />
                        </Dropdown.Menu>
                    </Dropdown>
                </Menu.Menu>
            </Menu>
        )
    }
}
export default Navbar;