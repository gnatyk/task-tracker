import React, { Component } from 'react';
import { Button, Modal, Form, Radio } from 'semantic-ui-react';
import UsersService from './services/UsersService';

class TaskEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nameTask :'',
      descrTask:'',
      assignee: '',
      status: '',
      developersList: []
    }
    this.setTaskProperty = this.setTaskProperty.bind(this);
    this.updateTask = this.updateTask.bind(this);
    this.getDevepopers = this.getDevepopers.bind(this);
  }
  setTaskProperty(name,value){
    let state = this.state;
    state[name] = value;
    this.setState(state); 
  }
  componentWillReceiveProps(newProps){
    let state = this.state;
    state.nameTask = newProps.task.name;
    state.descrTask = newProps.task.description;
    state.assignee = newProps.task.assigneeId;
    state.status = newProps.task.status;
    this.setState(state);
  }
  componentWillMount(){
    this.getDevepopers();
  }
  updateTask() {
    this.props.updateTask(this.state.nameTask,this.state.descrTask,
    this.state.assignee, this.state.status);
    this.props.onClose();
  }
  getDevepopers(){
    let self = this;
    UsersService.getUsersByRole('Developer', function(developers){
      let state = self.state;
      var optionSelect = developers.map(developer=>{
        return{
          key: developer.id,
          text: developer.name,
          value: developer.id
        }
      })
      state.developersList = optionSelect;
      self.setState(state);
    })

  }
  render() {
    return (
      <Modal size={'small'} open={this.props.isOpened} onClose={this.props.onClose}>
        <Modal.Header>
          Edit Task
          </Modal.Header>
        <Modal.Content>
          <Form>
            <Form.Group widths='equal'>
              <Form.Input label='Task summary' placeholder='Enter task summary' value={this.state.nameTask} onChange={(e)=>{this.setTaskProperty('nameTask',e.target.value)}}/>
              <Form.Select label='Assignee' options={this.state.developersList} value={this.state.assignee||''} onChange={(e,a)=>{this.setTaskProperty('assignee',a.value)}}  placeholder='Assignee' />
            </Form.Group>
            <Form.Group inline>
          <label>Status</label>
          <Form.Field control={Radio} label='Waiting' value='waiting' checked={this.state.status === 'waiting'} onChange={(e)=>{this.setTaskProperty('status','waiting')}} />
          <Form.Field control={Radio} label='Implementing' value='implementing' checked={this.state.status === 'implementing'} onChange={(e)=>{this.setTaskProperty('status','implementing')}} />
          <Form.Field control={Radio} label='Verifying' value='verifying' checked={this.state.status === 'verifying'} onChange={(e)=>{this.setTaskProperty('status','verifying')}} />
          <Form.Field control={Radio} label='Releasing' value='releasing' checked={this.state.status === 'releasing'} onChange={(e)=>{this.setTaskProperty('status','releasing')}} />
        </Form.Group>
            <Form.Group widths='equal'>
              <Form.TextArea label='Task description' placeholder='Enter task description' value={this.state.descrTask} onChange={(e)=>{this.setTaskProperty('descrTask',e.target.value)}} />
            </Form.Group>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button basic onClick={() => this.props.onClose()}>
            Cancel
              </Button>
          <Button positive onClick={this.updateTask} disabled={!this.state.nameTask || !this.state.descrTask}>
            Submit
            </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default TaskEditor;
