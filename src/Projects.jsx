import React, { Component } from 'react';
import { Button, Grid, Header, Icon, Message } from 'semantic-ui-react'
import ProjectsTable from './ProjectsTable';
import CreateProject from './CreateProject';
import UsersService from './services/UsersService';
import ProjectsService from './services/ProjectsService';


class Projects extends Component {
    constructor(props) {
        super(props);
        this.state = {
            createModalOpened: false,
            projects: [],
            currentUser: {}
        };
        this.openCreateModal = this.openCreateModal.bind(this);
        this.closeCreateModal = this.closeCreateModal.bind(this);
        this.createNewProject = this.createNewProject.bind(this);
        this.getProjects = this.getProjects.bind(this);
        this.deleteProject = this.deleteProject.bind(this);
    }
    createNewProject(name) {
        var self = this;
        ProjectsService.create(name, localStorage.userId, function () {
            self.getProjects(self.state.currentUser.role);
        })
    }
    openCreateModal() {
        let state = this.state;
        state.createModalOpened = true;
        this.setState(state);
    }
    closeCreateModal() {
        let state = this.state;
        state.createModalOpened = false;
        this.setState(state);
    }
    getProjects(role) {
        let self = this;
        this.setState({projects: []})
        if (role === 'Manager') {
            ProjectsService.getAllProjects(localStorage.userId, function (projects) {
                let state = self.state;
                state.projects = projects;
                self.setState(state);
            })
        }
        else if (role === 'Developer') {
            UsersService.getAssignedProjects(localStorage.userId, function (projects) {
                for (let i = 0; i < projects.length; i++) {
                    ProjectsService.getProjectById(projects[i].projectId, function (project) {
                        let state = self.state;
                        state.projects.push(project)
                        self.setState(state);
                    })
                }
            })
        }
    }
    componentWillMount() {
        let self = this;
        UsersService.getUserById(localStorage.userId, function (user) {
            self.setState({ currentUser: user })
            self.getProjects(user.role);
        })
    }
    deleteProject(id) {
        var self = this;
        ProjectsService.deleteProject(id, function () {
            self.getProjects();
        })
    }

    render() {
        return (
            <Grid >
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Header as='h2' icon textAlign='center'>
                            <Icon name='rocket' circular />
                            <Header.Content>
                                All Projects
                            </Header.Content>
                        </Header>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Column textAlign='right'>
                        {
                            this.state.currentUser.role === 'Manager' ?
                                <Button primary icon onClick={this.openCreateModal}>
                                    <Icon name='plus' /> Create Project
                        </Button>
                                : null
                        }
                        <CreateProject onCreate={this.createNewProject} isOpened={this.state.createModalOpened} onClose={this.closeCreateModal} />
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column textAlign='center' width={16}>
                        {
                            !this.state.projects.length ?
                                <Message info
                                    icon='rocket'
                                    header='You have no projects yet!'
                                />
                                :
                                <ProjectsTable data={this.state.projects} onDelete={this.deleteProject} />
                        }
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}

export default Projects;