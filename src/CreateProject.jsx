import React, { Component } from 'react';
import { Button, Modal, Form } from 'semantic-ui-react'

class CreateProject extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projectName: ''
    }
    this.getProjectName = this.getProjectName.bind(this);
    this.createNewProject = this.createNewProject.bind(this);
  }
  getProjectName(e) {
    let state = this.state;
    state.projectName = e.target.value;
    this.setState(state);
  }
  componentWillReceiveProps() {
    let state = this.state;
    state.projectName = '';
    this.setState(state);
  }
  createNewProject(){
    this.props.onCreate(this.state.projectName);
    this.props.onClose();
  }

  render() {
    return (
      <Modal size={'small'} open={this.props.isOpened} onClose={this.props.onClose}>
        <Modal.Header>
          Create new Project
          </Modal.Header>
        <Modal.Content>
          <Form as="div">
            <Form.Group widths='equal'>
              <Form.Input label='Project name' placeholder='Project name' value={this.state.projectName} onChange={this.getProjectName} />
            </Form.Group>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button basic onClick={() => this.props.onClose()}>
            Cancel
              </Button>
          <Button positive disabled={!this.state.projectName} onClick={this.createNewProject} >
            Submit
            </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default CreateProject;