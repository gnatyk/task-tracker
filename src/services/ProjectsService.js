import moment from 'moment';
import DbInitializer from './DbInitializer';

class ProjectsService {
  static getAllProjects(authorId, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('SELECT * FROM Projects WHERE authorId="' + authorId + '"', [], function (tx, results) {
        var arrayProjects = [];
        for (var i = 0; i < results.rows.length; i++) {
          arrayProjects.push(results.rows.item(i));
        }
        result(arrayProjects);
      })
    })
  }
  static getProjectById(id, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('SELECT * FROM Projects WHERE id="' + id + '"', [], function (tx, results) {
        let project = results.rows[0];
        result(project);
      })
    })
  }
  static create(name, authorId, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      var date = new Date();
      var dateString = moment(date).format('DD.MM.YYYY');
      var id = date.getTime();
      tx.executeSql('INSERT INTO Projects (id,name, authorId,date) VALUES ("' + id + '","' + name + '", "' + authorId + '","' + dateString + '")');
      result();
    });
  }
  static deleteProject(id, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('DELETE FROM Tasks WHERE projectId="' + id + '"');
      tx.executeSql('DELETE FROM Projects WHERE id="' + id + '"');
      result();
    })
  }
  static getProjectDevelopers(projectId, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('SELECT userId FROM UsersProjects WHERE projectId="' + projectId + '"', [], function (tx, results) {
        var arrayDevelopers = [];
        for (var i = 0; i < results.rows.length; i++) {
          arrayDevelopers.push(results.rows.item(i));
        }
        result(arrayDevelopers);
      })
    })
  }
  static addDeveloperToProject(userId, projectId, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('SELECT * FROM UsersProjects WHERE userId="' + userId + '" AND projectId="' + projectId + '"', [], function (tx, results) {
        if (results.rows.length === 0) {
          tx.executeSql('INSERT INTO UsersProjects (projectId, userId) VALUES ("' + projectId + '","' + userId + '")');
        }
        result();
      })
    })
  }
  static removeDeveloperFromProject(userId, projectId, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('UPDATE Tasks SET assigneeId="" WHERE assigneeId="' + userId + '"');
      tx.executeSql('DELETE FROM UsersProjects WHERE userId="' + userId + '" AND projectId="' + projectId + '"');
      result();
    })
  }
}

export default ProjectsService;