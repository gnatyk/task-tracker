import moment from 'moment';
import DbInitializer from './DbInitializer';

class CommentsService {
  static getAllComments(taskId, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('SELECT * FROM Comments WHERE taskId="' + taskId + '"', [], function (tx, results) {
        var arrayComments = [];
        for (var i = 0; i < results.rows.length; i++) {
          arrayComments.push(results.rows.item(i));
        }
        result(arrayComments);
      })
    })
  }
  static create(text, authorId, taskId, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      var date = new Date();
      var dateString = moment(date).format('DD.MM.YYYY hh:mm:ss');
      var id = date.getTime();
      tx.executeSql('INSERT INTO Comments (id,text,authorId,date,taskId) VALUES ("' + id + '","' + text + '", "'
        + authorId + '","' + dateString + '","' + taskId + '")');
      result();
    });
  }
  static deleteComment(id, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('DELETE FROM Comments WHERE id="' + id + '"');
      result();
    })
  }
  static updateComment(id, text, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('UPDATE Comments SET text="' + text + '" WHERE id="' + id + '"');
      result();
    })
  }
}


export default CommentsService;