let database = null;

class DbInitializer {
  static get database(){
    return database || (database = openDatabase("TaskTracker", "0.1", "", 200000));
  }
  static init() {
    let db = this.database;

    if (!db) { 
      alert("Failed to connect to database.");
      return;
     }
     db.transaction(function (tx){
       tx.executeSql('CREATE TABLE IF NOT EXISTS Users(id unique, name, password, email unique, role, photo)');
       tx.executeSql('CREATE TABLE IF NOT EXISTS Projects(id unique, name, authorId, date)');
       tx.executeSql('CREATE TABLE IF NOT EXISTS UsersProjects(userId, projectId)');
       tx.executeSql('CREATE TABLE IF NOT EXISTS Tasks(id unique, name, authorId, date, projectId, description, status, assigneeId)');
       tx.executeSql('CREATE TABLE IF NOT EXISTS Comments(id unique, text, authorId, date, taskId)');
     })
  }
}

export default DbInitializer;