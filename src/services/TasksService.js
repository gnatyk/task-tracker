import moment from 'moment';
import DbInitializer from './DbInitializer';

class TasksService {
  static getAllTasks(projectId, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('SELECT * FROM Tasks WHERE projectId="' + projectId + '"', [], function (tx, results) {
        var arrayTasks = [];
        for (var i = 0; i < results.rows.length; i++) {
          arrayTasks.push(results.rows.item(i));
        }
        result(arrayTasks);
      })
    })
  }
  static getAllTasksForUser(projectId, assigneeId, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('SELECT * FROM Tasks WHERE projectId="' + projectId
        + '" AND assigneeId="' + assigneeId + '"', [], function (tx, results) {
          var arrayTasks = [];
          for (var i = 0; i < results.rows.length; i++) {
            arrayTasks.push(results.rows.item(i));
          }
          result(arrayTasks);
        })
    })
  }
  static create(name, description, assigneeId, authorId, projectId, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      var date = new Date();
      var dateString = moment(date).format('DD.MM.YYYY');
      var id = date.getTime();
      tx.executeSql('INSERT INTO Tasks (id,name,assigneeId,description, authorId,date,status,projectId) VALUES ("' + id + '","' + name + '", "' + assigneeId + '","' + description + '","' + authorId + '","' + dateString + '", "waiting","' + projectId + '")');
      result();
    });
  }

  static deleteTask(id, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('DELETE FROM Tasks WHERE id="' + id + '"');
      tx.executeSql('DELETE FROM Comments WHERE taskId="' + id + '"');
      result();
    })
  }
  static getTaskById(taskId, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('SELECT * FROM Tasks WHERE id="' + taskId + '"', [], function (tx, results) {
        var len = results.rows.length;
        if (!len) {
          result(null);
        }
        else {
          result(results.rows[0]);
        }
      })
    })
  }
  static updateTask(id, name, assigneeId, status, description, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('UPDATE Tasks SET name="' + name + '", description="' + description +
        '",assigneeId="' + assigneeId + '",status="' + status + '" WHERE id="' + id + '"');
      result();
    })
  }
}

export default TasksService;