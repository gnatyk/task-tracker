import DbInitializer from './DbInitializer';

class UsersService {
  static getUserById(id, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('SELECT * FROM Users WHERE id="' + id + '"', [], function (tx, results) {
        var len = results.rows.length;
        if (!len) {
          result(null);
        }
        else {
          result(results.rows[0]);
        }
      })
    })
  }
  static getUsersByRole(role, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('SELECT * FROM Users WHERE role="' + role + '"', [], function (tx, results) {
        var arrayProjects = [];  
        for(var i=0; i < results.rows.length; i++){
          arrayProjects.push(results.rows.item(i));
        }
        result(arrayProjects);
      })
    })
  }
  static updateUser(userId, name, photo, result){
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('UPDATE Users SET name="'+name+ '", photo="'+photo+'" WHERE id="'+userId+'"');
      result();
    })
  }
   static getAssignedProjects(userId, result) {
    let db = DbInitializer.database;
    db.transaction(function (tx) {
      tx.executeSql('SELECT projectId FROM UsersProjects WHERE userId="' +userId + '"', [], function (tx, results) {
        var arrayProjects = [];
        for (var i = 0; i < results.rows.length; i++) {
          arrayProjects.push(results.rows.item(i));
        }
        result(arrayProjects);
      })
    })
  }
}

export default UsersService;