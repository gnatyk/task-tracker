import React, { Component } from 'react';
import { Icon, Table, Button, Grid, Message } from 'semantic-ui-react';
import { browserHistory } from 'react-router';


class TasksTable extends Component {
    onTaskClick(projectId, taskId) {
        browserHistory.push("/project/" +projectId+ "/task/"+taskId);
    }
     deleteTask(id,e){
        e.stopPropagation();
        this.props.onDelete(id);
    }
    render() {
        return (
            <Grid>
                <Grid.Row>
                    <Grid.Column>
                        {
                            !this.props.tasks.length?
                            <Message positive
                                icon='tasks'
                                header='This project has no tasks yet!'
                                content='Press button above to create one.'
                            />                        
                            :
                            <Table celled selectable>
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell>Task Name</Table.HeaderCell>
                                        <Table.HeaderCell>Status</Table.HeaderCell>
                                        <Table.HeaderCell>Creation Date</Table.HeaderCell>
                                        <Table.HeaderCell width={1}></Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                    {
                                        this.props.tasks.map((task) => {
                                            return (
                                                <Table.Row onClick={() => { this.onTaskClick(task.projectId,task.id) }} key={task.id} className="clickable">
                                                    <Table.Cell>{task.name}</Table.Cell>
                                                    <Table.Cell>{task.status}</Table.Cell>
                                                    <Table.Cell><Icon name="calendar" /> {task.date}</Table.Cell>
                                                    <Table.Cell textAlign="center"><Button icon negative size="mini" onClick={(e) => {this.deleteTask(task.id,e)}}><Icon name="trash" /></Button></Table.Cell>
                                                </Table.Row>
                                            )
                                        })
                                    }
                                </Table.Body>
                            </Table>
                        }
                    </Grid.Column>
                </Grid.Row>
            </Grid>

        );
    }

}

export default TasksTable;