import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Projects from './Projects';
import Project from './Project';
import TasksTable from './TasksTable';
import DevelopersList from './DevelopersList';
import Task from './Task'
import { Router, Route, browserHistory, Redirect } from 'react-router'
import './style.css'
import DbInitializer from './services/DbInitializer';
import Auth from './Auth';
import Profile from './Profile';

DbInitializer.init();

ReactDOM.render(
  localStorage.userId?
   <Router history={browserHistory}>
     <Redirect from="/project" to="/project/tasks" />
     <Redirect from="/" to="/projects" />
    <Route path="/" component={App}>
      <Route path="/profile" component={Profile}></Route>
      <Route path="/projects" component={Projects}>
      </Route>
      <Route path="/project" component={Project}>
        <Route path="/project/:id/tasks" component={TasksTable}></Route>
        <Route path="/project/:id/developers" component={DevelopersList}></Route>
      </Route>
      <Route path="/project/:projectId/task/:taskId" component={Task}></Route>
    </Route>
    </Router>
    :
    <Auth />,
  document.getElementById('root')
);
