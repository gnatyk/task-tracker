import React, { Component } from 'react';
import TaskInfo from './TaskInfo';
import TaskComments from './TaskComments';
import { Segment } from 'semantic-ui-react';

class Task extends Component {
    render() {
        return (
            <div>
                <Segment>
                    <TaskInfo taskId={this.props.params.taskId}/>
                    <TaskComments taskId={this.props.params.taskId}/>
                </Segment>
                <br />
            </div>
        );
    }
}

export default Task;