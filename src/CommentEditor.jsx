import React, { Component } from 'react';
import { Button, Modal, Form } from 'semantic-ui-react';

class CommentEditor extends Component {
  constructor(props) {
    super(props);
    this.state= {
      updateTextComment: ''
    }
    this.onUpdateStateComment = this.onUpdateStateComment.bind(this);
    this.updateComment = this.updateComment.bind(this);
  }
  onUpdateStateComment(e){
    let state = this.state;
    state.updateTextComment = e.target.value;
    this.setState(state);
  }
  componentWillReceiveProps(newProps){
    let state= this.state;
    state.updateTextComment = newProps.comment.text;
    this.setState(state);
  }
  updateComment(){
    this.props.updateComment(this.props.comment.id, this.state.updateTextComment);
    this.props.onClose();
  }
  render() {
    
    return (
      <Modal size={'small'} open={this.props.isOpened} onClose={this.props.onClose}>
        <Modal.Header>
          Edit comment
          </Modal.Header>
        <Modal.Content>
          <Form>
            <Form.Group widths='equal'>
              <Form.TextArea label='Comment text' placeholder='Comment text' value={this.state.updateTextComment} onChange={this.onUpdateStateComment} />
            </Form.Group>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button basic onClick={() => this.props.onClose()}>
            Cancel
              </Button>
          <Button positive disabled={!this.state.updateTextComment} onClick={this.updateComment} >
            Submit
            </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default CommentEditor;