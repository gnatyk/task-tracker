import React, { Component } from 'react'
import { Button, Comment, Form, Header, Icon } from 'semantic-ui-react';
import CommentsService from './services/CommentsService';
import UsersService from './services/UsersService';
import CommentEditor from './CommentEditor';


class TaskComments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comments: [],
      usersMap: {},
      textComment: '',
      commentIsOpened: false,
      selectedComment: {}
    }
    this.setTextComment = this.setTextComment.bind(this);
    this.setAuthor = this.setAuthor.bind(this);
    this.setComments = this.setComments.bind(this);
    this.createComment = this.createComment.bind(this);
    this.deleteComment = this.deleteComment.bind(this);
    this.modalClose = this.modalClose.bind(this);
    this.modalOpen = this.modalOpen.bind(this);
    this.updateComment = this.updateComment.bind(this);
  }
  componentWillMount() {
    this.setComments();
  }
  setComments() {
    let self = this;
    CommentsService.getAllComments(this.props.taskId, function (comments) {
      let state = self.state;
      self.setAuthor(comments);
      state.comments = comments;
      self.setState(state);
    })
  }
  setAuthor(comments) {
    let self = this;
    for (var i = 0; i < comments.length; i++) {
      UsersService.getUserById(comments[i].authorId, function (user) {
        let state = self.state;
        state.usersMap[user.id] = user;
        self.setState(state);
      })
    }
  }
  setTextComment(e) {
    let state = this.state;
    state.textComment = e.target.value;
    this.setState(state);
  }
  createComment() {
    let self = this;
    CommentsService.create(this.state.textComment, localStorage.userId,
      this.props.taskId,
      function () {
        self.setComments();
        let state = self.state;
        state.textComment = '';
        self.setState(state);
      })
  }
  deleteComment(commentId) {
    let self = this;
    CommentsService.deleteComment(commentId, function () {
      self.setComments();
    })
  }
  modalOpen(comment) {
    this.setState({ commentIsOpened: true, selectedComment: comment });
  }
  modalClose() {
    this.setState({ commentIsOpened: false, selectedComment: {} });
  }
  updateComment(id, text) {
    let self = this;
    CommentsService.updateComment(id, text, function () {
      self.setComments();
    });
  }

  render() {
    let defaultImage = 'http://react.semantic-ui.com/assets/images/wireframe/white-image.png';
    return (
      <Comment.Group size='small'>
        <Header as='h3' dividing>Comments</Header>
        {
          this.state.comments.map(comment => {
            let author = this.state.usersMap[comment.authorId] || {};
            return (
              <Comment key={comment.id}>
                <Comment.Avatar src={author.photo || defaultImage} />
                <Comment.Content>
                  <Comment.Author as='a'>{author.name}</Comment.Author>
                  <Comment.Metadata>
                    <div>{comment.date}</div>
                  </Comment.Metadata>
                  {
                    localStorage.userId === author.id ?
                      <Button.Group icon floated="right" basic size="mini">
                        <Button onClick={() => { this.modalOpen(comment) }}><Icon name="pencil" /></Button>
                        <Button onClick={() => { this.deleteComment(comment.id) }}><Icon color="red" name="trash" /></Button>
                      </Button.Group>
                      : null
                  }
                  <Comment.Text>{comment.text}</Comment.Text>
                  <Comment.Actions>
                  </Comment.Actions>
                </Comment.Content>
              </Comment>
            );
          })
        }
        <CommentEditor isOpened={this.state.commentIsOpened} onClose={this.modalClose} comment={this.state.selectedComment} updateComment={this.updateComment} />
        <Form reply onSubmit={e => e.preventDefault()} >
          <Form.TextArea placeholder="Enter your comment here" autoHeight value={this.state.textComment} onChange={this.setTextComment} />
          <Button disabled={!this.state.textComment.length} content='Add Reply' labelPosition='left' icon='edit' primary floated="right" onClick={this.createComment} />
          <br />
          <br />
        </Form>
      </Comment.Group>
    );
  }
}

export default TaskComments;