import React, { Component } from 'react';
import { Header, Image, Container, Form, Button, Icon } from 'semantic-ui-react'
import UsersService from './services/UsersService';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: {},
      myName: '',
      myPhotoUrl: ''
    }
    this.getCurrentUser = this.getCurrentUser.bind(this);
    this.update = this.update.bind(this);
    this.cancelChanges = this.cancelChanges.bind(this);
  }
  getCurrentUser() {
    let myId = localStorage.userId;
    let self = this;
    UsersService.getUserById(myId, function (user) {
      self.setState({ currentUser: user, myName: user.name, myPhotoUrl: user.photo });
    })
  }
  componentWillMount() {
    this.getCurrentUser();
  }
  changeState(name, value) {
    let state = this.state;
    state[name] = value;
    this.setState(state);
  }

  update() {
    let self = this;
    UsersService.updateUser(localStorage.userId, this.state.myName, this.state.myPhotoUrl, function () {
      self.getCurrentUser();
    })
  }

  cancelChanges(){
    this.setState({
      myName: this.state.currentUser.name,
      myPhotoUrl: this.state.currentUser.photo
    });
  }

  render() {
    let imageSrc = this.state.currentUser.photo || 'http://react.semantic-ui.com/assets/images/wireframe/white-image.png';
    return (
      <Container text>
        <Header as='h2' textAlign="center">
          <Image shape='circular' src={imageSrc} />
          {' '} {this.state.currentUser.name}
        </Header>
        <br />
        <Form as="div">
          <Form.Group widths='equal'>
            <Form.Input label="Name" placeholder='Enter Name' value={this.state.myName} onChange={(e) => this.changeState('myName', e.target.value)} />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Input label="Photo URL" placeholder='Enter Photo URL' type="text" value={this.state.myPhotoUrl} onChange={(e) => this.changeState('myPhotoUrl', e.target.value)} />
          </Form.Group>
          <Button
            onClick={this.update}
            disabled={
              (this.state.myName === this.state.currentUser.name &&
                this.state.myPhotoUrl === this.state.currentUser.photo) ||
              !this.state.myName
            } positive icon floated="right"><Icon name="save" /> Save changes</Button>
          <Button primary basic onClick={this.cancelChanges} icon floated="right"><Icon name="cancel" /> Cancel</Button>
        </Form>
      </Container>
    );
  }
}

export default Profile;