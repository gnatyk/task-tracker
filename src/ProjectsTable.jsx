import React, { Component } from 'react';
import { Icon, Table, Button } from 'semantic-ui-react';
import { browserHistory } from 'react-router';



class ProjectsTable extends Component {
    constructor(props){
        super(props);
        this.deleteProject = this.deleteProject.bind(this);
    }

    onProjectClick(id) {
        browserHistory.push('/project/'+id+'/tasks');
    }
    deleteProject(id,e){
        e.stopPropagation();
        this.props.onDelete(id);
    }
    render() {
        return (
            <Table celled selectable>
                <Table.Header>
                    <Table.Row >
                        <Table.HeaderCell>Project Name</Table.HeaderCell>
                        <Table.HeaderCell>Creation Date</Table.HeaderCell>
                        <Table.HeaderCell width={1}></Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body >
                    {
                        this.props.data.map(project => {
                            return (
                                <Table.Row onClick={()=> this.onProjectClick(project.id)} key={project.id} className="clickable">
                                    <Table.Cell>{project.name}</Table.Cell>
                                    <Table.Cell><Icon name="calendar" /> {project.date}</Table.Cell>
                                    <Table.Cell textAlign="center"><Button icon negative size="mini" onClick={(e)=>this.deleteProject(project.id,e)} ><Icon name="trash" /></Button></Table.Cell>
                                </Table.Row>
                            );
                        })
                    }
                </Table.Body>
            </Table>
        )
    }
}

export default ProjectsTable;