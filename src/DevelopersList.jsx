import React, { Component } from 'react';
import { Dropdown, List, Image, Grid, Container, Segment, Button, Icon,Message } from 'semantic-ui-react';
import ProjectsService from './services/ProjectsService';
import UsersService from './services/UsersService';



class DevelopersList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            developersListAssignee: [],
            developersListAll: [],
            selectedDeveloper: ''
        }
        this.setSelectedDeveloper = this.setSelectedDeveloper.bind(this);
        this.getProjectDevelopers = this.getProjectDevelopers.bind(this);
        this.addDeveloper = this.addDeveloper.bind(this);
    }
    getProjectDevelopers() {
        let self = this;
        this.setState({ developersListAssignee: [] })
        ProjectsService.getProjectDevelopers(self.props.params.id, function (developersList) {
            for (let i = 0; i < developersList.length; i++) {
                UsersService.getUserById(developersList[i].userId, function (developer) {
                    let state = self.state;
                    state.developersListAssignee.push(developer);
                    self.setState(state);
                })
            }
        });
    }
    componentWillMount() {
        let self = this;
        this.getProjectDevelopers();
        UsersService.getUsersByRole('Developer', function (developers) {
            let state = self.state;
            state.developersListAll = developers;
            self.setState(state);
        })
    }
    setSelectedDeveloper(e, option) {
        let state = this.state;
        state.selectedDeveloper = option.value;
        this.setState(state);
    }
    addDeveloper() {
        let self = this;
        ProjectsService.addDeveloperToProject(this.state.selectedDeveloper, this.props.params.id, function () {
            self.getProjectDevelopers();
        })
        this.setState({ selectedDeveloper: '' })
    }
    removeDeveloper(userId) {
        let self = this;
        ProjectsService.removeDeveloperFromProject(userId, this.props.params.id, function () {
            self.getProjectDevelopers();
        })
    }
    render() {
        let defaultImage = 'http://react.semantic-ui.com/assets/images/wireframe/white-image.png';
        return (
            <Segment>
                <Container text>
                    <Grid>
                        {
                            this.props.project.authorId === localStorage.userId ?
                                <Grid.Row>
                                    <Grid.Column textAlign='left' width={6}>
                                        <Dropdown placeholder='State' search selection
                                            options={
                                                this.state.developersListAll.map(developer => {
                                                    return {
                                                        key: developer.id,
                                                        value: developer.id,
                                                        text: developer.name
                                                    }
                                                })
                                            }
                                            value={this.state.selectedDeveloper}
                                            onChange={this.setSelectedDeveloper} />
                                    </Grid.Column>
                                    <Grid.Column width={6}>
                                        <Button disabled={!this.state.selectedDeveloper} icon size={'large'} basic positive onClick={this.addDeveloper}><Icon name='plus' />Add</Button>
                                    </Grid.Column>
                                </Grid.Row>
                                : null
                        }
                        <Grid.Row className='list-developers'>
                            <Grid.Column width={15}>
                                {
                                    !this.state.developersListAssignee.length ?
                                        <Message positive
                                            icon='users'
                                            header='This project has no developers yet!'
                                        />
                                        :
                                        <List>
                                            {
                                                this.state.developersListAssignee.map((developer => {
                                                    return (
                                                        <List.Item key={developer.id}>
                                                            {
                                                                this.props.project.authorId === localStorage.userId ?
                                                                    <List.Content floated="right">
                                                                        <Button basic negative icon="remove" onClick={(e) => { this.removeDeveloper(developer.id) }} />
                                                                    </List.Content>
                                                                    : null
                                                            }

                                                            <Image avatar src={developer.photo || defaultImage} />
                                                            <List.Content>
                                                                <List.Header as='a'>{developer.name}
                                                                </List.Header>
                                                                <List.Description>Has an access to the project</List.Description>
                                                            </List.Content>
                                                        </List.Item>
                                                    );
                                                }))
                                            }
                                        </List>
                                }
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container>
            </Segment>
        );
    }
}


export default DevelopersList;