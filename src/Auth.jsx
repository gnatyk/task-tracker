import React, { Component } from 'react';
import { Form, Container, Segment, Radio, Header, Icon, Button, Grid} from 'semantic-ui-react';
import AuthService from './services/AuthService';
class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginEmail: '',
      regEmail: '',
      loginPassword: '',
      regPassword: '',
      regRole: 'Manager',
      regName: ''
    }
    this.changeState = this.changeState.bind(this);
    this.getLogForm = this.getLogForm.bind(this);
    this.getRegForm = this.getRegForm.bind(this);
  }
  changeState(name, value){
    let state = this.state;
    state[name] = value;
    this.setState(state);
  }

  getRegForm() {
    if (!this.state.regEmail || !this.state.regPassword) {
      return;
    }
    AuthService.register(this.state.regEmail, this.state.regPassword, this.state.regRole,this.state.regName,
    function(userId){
      localStorage.userId = userId;
      window.location.reload();
    },
    function(data){
      alert(data);

    });
  }
  getLogForm() {
    if (!this.state.loginEmail || !this.state.loginPassword) {
      return;
    }
    AuthService.login(this.state.loginEmail, this.state.loginPassword,
     function(userId){
       localStorage.userId = userId;
       window.location.reload();
     },
      function(data){
       alert(data);
     })
  }

  render() {
    return (
      <Container text className="auth">
        <br />
        <Grid centered>
          <Grid.Column width={14}>
            <Header as='h2' icon textAlign='center'>
              <Icon name='key' circular />
              <Header.Content>
                Authorization
              </Header.Content>
              <Header.Subheader>
                Use your credentials to log in or create new account
              </Header.Subheader>
            </Header>
            <Segment>
              <Grid divided>
                <Grid.Row>
                  <Grid.Column width={8}>
                    <Header as='h4' textAlign='center'>
                      <Header.Content>
                        Log In
                      </Header.Content>
                    </Header>
                    <Form as="div">
                      <Form.Group widths='equal'>
                        <Form.Input placeholder='Email' value={this.state.loginEmail} onChange={(e) => this.changeState('loginEmail',e.target.value)} />
                      </Form.Group>
                      <Form.Group widths='equal'>
                        <Form.Input placeholder='Password' type="password" value={this.state.loginPassword} onChange={(e) => this.changeState('loginPassword',e.target.value)} />
                      </Form.Group>
                    </Form>
                  </Grid.Column>
                  <Grid.Column width={8}>
                    <Header as='h4' textAlign='center'>
                      <Header.Content>
                        Registration
                      </Header.Content>
                    </Header>
                    <Form as="div">
                      <Form.Group widths='equal'>
                        <Form.Input type="text" placeholder='Name' value={this.state.regName} onChange={(e) => this.changeState('regName',e.target.value)} />
                      </Form.Group>
                      <Form.Group widths='equal'>
                        <Form.Input type="email" placeholder='Email' value={this.state.regEmail} onChange={(e) => this.changeState('regEmail',e.target.value)} />
                      </Form.Group>
                      <Form.Group widths='equal'>
                        <Form.Input placeholder='Password' type="password" value={this.state.regPassword} onChange={(e) => this.changeState('regPassword',e.target.value)} />
                      </Form.Group>
                      <Form.Group>
                        <Form.Field>
                          <Radio
                            label='Manager'
                            name='radioGroup'
                            checked={this.state.regRole === 'Manager'}
                            onChange={(e) => this.changeState('regRole',e.target.innerText)}
                          />
                          <Radio
                            label='Developer'
                            name='radioGroup'
                            checked={this.state.regRole === 'Developer'}
                            onChange={(e) => this.changeState('regRole',e.target.innerText)}
                          />
                        </Form.Field>
                      </Form.Group>
                    </Form>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={8}>
                    <Button
                      fluid
                      positive
                      floated="right" 
                      basic
                      size="small"
                      className="submit"
                      disabled={!this.state.loginEmail || !this.state.loginPassword}
                      onClick={this.getLogForm}
                    >
                      Log in
                      </Button>
                  </Grid.Column>
                  <Grid.Column width={8}>
                    <Button
                      fluid
                      positive
                      floated="right"
                      basic
                      size="small"
                      onClick={this.getRegForm}
                      className="submit"
                      disabled={!this.state.regEmail || !this.state.regPassword || !this.state.regName}
                    >
                      Register
                      </Button>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
          </Grid.Column>
        </Grid>
      </Container>
    );
  }
}

export default Auth;